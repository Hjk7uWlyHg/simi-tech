#pragma once

#include "okapi/impl/device/motor/motor.hpp"
#include "okapi/impl/device/motor/motorGroup.hpp"

extern okapi::Motor puncherMtr;
extern okapi::MotorGroup leftGroup;
extern okapi::MotorGroup rightGroup;
extern okapi::Motor armMtr;
extern okapi::Motor intakeMtr;
extern okapi::Motor anglerMtr;

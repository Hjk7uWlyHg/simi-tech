#include "config.hpp"

okapi::Motor puncherMtr(-9);
okapi::MotorGroup leftGroup({-8,-6});
okapi::MotorGroup rightGroup({7,5});
okapi::Motor armMtr(4);
okapi::Motor intakeMtr(3);
okapi::Motor anglerMtr(1);

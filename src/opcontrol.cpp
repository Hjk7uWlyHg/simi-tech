#include "main.h"

/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */
void opcontrol() {
	okapi::Controller masterController(okapi::ControllerId::master);
	okapi::Controller partnerController(okapi::ControllerId::partner);
	auto chassis = okapi::ChassisControllerFactory::create(leftGroup, rightGroup);

	okapi::ControllerButton brakeBtn = masterController[okapi::ControllerDigital::R2];
	bool isBraking;

	okapi::ControllerButton anglerBtn = masterController[okapi::ControllerDigital::A];
	bool anglerDoubleShot;
	anglerMtr.tarePosition();
	auto anglerController = okapi::AsyncControllerFactory::posIntegrated(anglerMtr);
	puncherMtr.tarePosition();
	auto puncherController = okapi::AsyncControllerFactory::posIntegrated(puncherMtr);

	while (true) {
		if (brakeBtn.changedToPressed()) {
			isBraking = !isBraking;
		}
		if (isBraking) {
			leftGroup.setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
			leftGroup.moveVelocity(0);
			rightGroup.setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
			rightGroup.moveVelocity(0);
		} else {
			leftGroup.setBrakeMode(okapi::AbstractMotor::brakeMode::coast);
			rightGroup.setBrakeMode(okapi::AbstractMotor::brakeMode::coast);
			chassis.driveVector(
				masterController.getAnalog(okapi::ControllerAnalog::leftY),
				masterController.getAnalog(okapi::ControllerAnalog::leftX)
			);
		}

		intakeMtr.moveVelocity(200 * masterController.getAnalog(okapi::ControllerAnalog::rightY));
		puncherMtr.moveVelocity(200 * masterController.getDigital(okapi::ControllerDigital::R1));
		anglerMtr.moveVelocity(
			200 * masterController.getDigital(okapi::ControllerDigital::L1)
			- 200 * masterController.getDigital(okapi::ControllerDigital::L2)
		);
		armMtr.moveVelocity(200 * partnerController.getAnalog(okapi::ControllerAnalog::rightY));

		if (anglerBtn.changedToPressed()) {
			//TODO: Change these values
			anglerController.setTarget(0);
			anglerController.waitUntilSettled();
			puncherController.setTarget(puncherMtr.getPosition() + 200);
			puncherController.waitUntilSettled();
			anglerController.setTarget(100);
			anglerController.waitUntilSettled();
			puncherController.setTarget(puncherMtr.getPosition() + 200);
			puncherController.waitUntilSettled();
			anglerController.setTarget(0);
			anglerController.waitUntilSettled();
		}
	}
}
